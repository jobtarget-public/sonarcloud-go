package main

import (
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"time"
)

// statusHandler is an http.Handler that writes an empty response using itself
// as the response status code.
type statusHandler int

func (h *statusHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
    w.WriteHeader(int(*h))
}

func TestIsTagged(t *testing.T) {
	// Set up a fake "Google Code" web server reporting 404 not found.
	status := statusHandler(http.StatusNotFound)
	s := httptest.NewServer(&status)
	defer s.Close()

	if isTagged(s.URL) {
		t.Fatal("isTagged == true, want false")
	}

	// Change fake server status to 200 OK and try again.
	status = http.StatusOK

	if !isTagged(s.URL) {
		t.Fatal("isTagged == false, want true")
	}
}

func TestIntegration(t *testing.T) {
	status := statusHandler(http.StatusNotFound)
	ts := httptest.NewServer(&status)
	defer ts.Close()

	// Replace the pollSleep with a closure that we can block and unblock.
	sleep := make(chan bool)
	pollSleep = func(time.Duration) {
		sleep <- true
		sleep <- true
	}

	// Replace pollDone with a closure that will tell us when the poller is
	// exiting.
	done := make(chan bool)
	pollDone = func() { done <- true }

	// Put things as they were when the test finishes.
	defer func() {
		pollSleep = time.Sleep
		pollDone = func() {}
	}()

	s := NewServer("1.x", ts.URL, 1*time.Millisecond)

	<-sleep // Wait for poll loop to start sleeping.

	// Make first request to the server.
	r, _ := http.NewRequest("GET", "/", nil)
	w := httptest.NewRecorder()
	s.ServeHTTP(w, r)
	if b := w.Body.String(); !strings.Contains(b, "No.") {
		t.Fatalf("body = %s, want no", b)
	}

	status = http.StatusOK

	<-sleep // Permit poll loop to stop sleeping.
	<-done  // Wait for poller to see the "OK" status and exit.

	// Make second request to the server.
	w = httptest.NewRecorder()
	s.ServeHTTP(w, r)
	if b := w.Body.String(); !strings.Contains(b, "YES!") {
		t.Fatalf("body = %q, want yes", b)
	}
}

func TestNewServer(t *testing.T) {
	// Test NewServer with different versions and URLs.
	version := "1.5"
	url := "https://example.com"
	period := 1 * time.Second

	s := NewServer(version, url, period)

	if s.version != version {
		t.Errorf("Expected version %s, got %s", version, s.version)
	}

	if s.url != url {
		t.Errorf("Expected URL %s, got %s", url, s.url)
	}

	if s.period != period {
		t.Errorf("Expected period %s, got %s", period, s.period)
	}
}

func TestServeHTTP(t *testing.T) {
	// Test ServeHTTP with different scenarios.
	s := &Server{
		version: "1.5",
		url:     "https://example.com",
		period:  1 * time.Second,
		yes:     true, // Set to true for testing purposes
	}

	r, _ := http.NewRequest("GET", "/", nil)
	w := httptest.NewRecorder()
	s.ServeHTTP(w, r)

	if w.Code != http.StatusOK {
		t.Errorf("Expected status code %d, got %d", http.StatusOK, w.Code)
	}

	// Test the "No" scenario.
	s.yes = false
	w = httptest.NewRecorder()
	s.ServeHTTP(w, r)
	if w.Code != http.StatusOK {
		t.Errorf("Expected status code %d, got %d", http.StatusOK, w.Code)
	}
}

func TestIsTaggedWithError(t *testing.T) {
	// Test isTagged when an error occurs during HTTP request.
	errorMessage := "Test error message"
	errorHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		http.Error(w, errorMessage, http.StatusInternalServerError)
	})
	ts := httptest.NewServer(errorHandler)
	defer ts.Close()

	if isTagged(ts.URL) {
		t.Fatal("isTagged == true, want false")
	}
}

// func TestIsTaggedWithInvalidURL(t *testing.T) {
// 	// Test isTagged with an invalid URL.
// 	invalidURL := "invalid-url"
// 	if isTagged(invalidURL) {
// 		t.Fatal("isTagged == true, want false")
// 	}
// }
