# Use the official Go image as the base image
FROM golang:1.17-alpine

# Set the working directory inside the container
WORKDIR /app

# Copy the main.go file into the container
COPY main.go .

# Build the Go application
RUN go build -o outyet main.go

# Expose the port the application will run on
EXPOSE 8080

# Command to run the application
CMD ["./outyet"]
